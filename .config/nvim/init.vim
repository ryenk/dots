""""""""""""""""""""""""""""""""""
"   Usability
""""""""""""""""""""""""""""""""""
set smarttab
"set tabstop=4
set shiftwidth=4
" use spaces instead of tabs
set expandtab
"let mapleader=","
map <SPACE> <Leader>

""""""""""""""""""""""""""""""""""
" Keybinds
""""""""""""""""""""""""""""""""""
" Noh after search
nnoremap <silent> <leader>h :nohlsearch<CR>
" Copy From Keyboard
vnoremap <leader>y "+y
" Paste From Keyboard
vnoremap <leader>p "+p
" j/k qill move virtual lines (lines that wrap)
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

inoremap jj <ESC>
""""""""""""""""""""""""""""""""""
"   Interface
""""""""""""""""""""""""""""""""""
"" Line number settings
set number relativenumber
augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave    * set relativenumber
    autocmd BufEnter,FocusLost,InsertEnter      * set norelativenumber
augroup end
set number
" Make comments italic
highlight comment cterm=italic gui=italic


""""""""""""""""""""""""""""""""""
"   Plugins
""""""""""""""""""""""""""""""""""
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dir https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')
    Plug 'jiangmiao/auto-pairs'
    Plug 'reedes/vim-pencil'
    Plug 'dhruvasagar/vim-table-mode'
    Plug 'ctrlpvim/ctrlp.vim'
    "Plug 'ayu-theme/ayu-vim' " colorscheme
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'Yggdroot/indentLine' " line indentation visualization
    Plug 'scrooloose/nerdtree' " side directory viewer
call plug#end()

set notermguicolors

"--------- bling/vim-airline settings -------------
" always show statusbar
set laststatus=2
" show paste if in paste mode
let g:airline_detect_paste=1
" use powerline font extras (arrows)
let g:airline_powerline_fonts=1
" show airline for tabs
let g:airline#extension#tabline#enabled=1
let g:airline_theme='minimalist'
" --------- indentLine settings -------------------
"let g:indentLine_showFirstLevelIndent=1
"let g:indentLine_setColors=0

let g:pencil#wrapModeDefault = 'soft'
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = ''
