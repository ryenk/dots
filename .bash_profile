#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH="$PATH:$HOME/.bin"
export PATH="$PATH:$HOME/.emacs.d/bin"

# MPD daemon start (if no other user instance exists)
[ ! -s ~/.config/mpd/pid ] && mpd

;if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
;	exec startx
;fi
