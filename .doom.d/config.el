;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)
(set-frame-parameter (selected-frame) 'alpha '(90 90))
(add-to-list 'default-frame-alist '(alpha 90 90))
;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'nil)
(set-fringe-mode '(40 . 40))
;(set-frame-parameter nil 'internal-border-width 30)

;(setq doom-leader-key "."
;      doom-localleader-key "\\")
;----------------------------------------------------------------------------
;  Font
;----------------------------------------------------------------------------
(setq doom-font (font-spec :family "Fira Code" :size 16))
;; Hangul Font
(set-fontset-font "fontset-default" 'hangul (font-spec :family "KBIZgo R"))
(setq face-font-rescale-alist
  '(
    (".*KBIZgo R.*" . 1.1)
    (".*designhouse.*" . 1.2)
))
;----------------------------------------------------------------------------
; Hangul Input
;----------------------------------------------------------------------------
(setq default-korean-keyboard "korean-hangul3f")
(setq default-input-method "korean-hangul3f")
(bind-key "S-SPC" 'toggle-input-method)
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
; copied from https://tecosaur.github.io/emacs-config/config.html
;----------------------------------------------------------------------------
(setq-default
 delete-by-moving-to-trash t                      ; Delete files to trash
 tab-width 4                                      ; Set width for tabs
 uniquify-buffer-name-style 'forward              ; Uniquify buffer names
 window-combination-resize t                      ; take new window space from all other windows (not just current)
 x-stretch-cursor t)                              ; Stretch cursor to the glyph width

(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      auto-save-default t                         ; Nobody likes to loose work, I certainly don't
      inhibit-compacting-font-caches t            ; When there are lots of glyphs, keep them in memory
      truncate-string-ellipsis "…")               ; Unicode ellispis are nicer than "...", and also save /precious/ space

(delete-selection-mode 1)                         ; Replace selection when inserting text
(display-time-mode 1)                             ; Enable time in the mode-line
(unless (equal "Battery status not available"
               (battery))
  (display-battery-mode 1))                       ; On laptops it's nice to know how much power you have
(global-subword-mode 1)                           ; Iterate through CamelCase words

;;; cutomisation using seperate file
(setq-default custom-file (expand-file-name ".custom.el" doom-private-dir))
(when (file-exists-p custom-file)
  (load custom-file))

;----------------------------------------------------------------------------
; flyspell
;----------------------------------------------------------------------------
(set-company-backend! '(text-mode
                        markdown-mode)
  '(:seperate company-ispell
              company-files
              company-yasnippet))

(after! flyspell (require 'flyspell-lazy) (flyspell-lazy-mode 1))



(setq-default ispell-program-name "hunspell")
(setq ispell-really-hunspell t)

;; 사전 목록에 한국어("korean") 추가
;(setq ispell-local-dictionary-alist
;      '(("korean"
;         "[가-힣]"
;         "[^가-힣]"
;         "[0-9a-zA-Z]" nil
;         ("-d" "ko_KR")
;         nil utf-8)))

(setq ispell-dictionary "ko_KR")
(setq ispell-personal-dictionary (expand-file-name ".ispell_personal" doom-private-dir))

(defun flyspell-check-next-highlighted-word ()
  "Custom function to spell check next highlighted word"
  (interactive)
  (flyspell-goto-next-error)
  (ispell-word)
)
(map! :map global-map
      "C-<f7>"  #'ispell-word
      "M-<f7>"  #'flyspell-check-previous-highlighted-word
      "<f7>"    #'flyspell-check-next-highlighted-word)

;(global-set-key (kbd "<f7>") 'ispell-word)
;(global-set-key (kbd "M-<f7>") 'flyspell-check-previous-highlighted-word)

;(global-set-key (kbd "C-<f7>") 'flyspell-check-next-highlighted-word)

(defun fd-switch-dictionary()
(interactive)
(let* ((dic ispell-current-dictionary)
 (change (if (string= dic "ko_KR") "en_US" "ko_KR")))
  (ispell-change-dictionary change)
  (message "Dictionary switched from %s to %s" dic change)
  ))
(map! :map global-map
      "<f6>"    #'fd-switch-dictionary)
;(global-set-key (kbd "<f6>")   'fd-switch-dictionary)

(after! org (add-hook 'org-mode-hook 'turn-on-flyspell))
;----------------------------------------------------------------------------


(setq ivy-read-action-function #'ivy-hydra-read-action)

(setq yas-triggers-in-field t)

;----------------------------------------------------------------------------
;  Key Bind
;----------------------------------------------------------------------------
(map! :leader
      (:desc "no highlight" "h" #'evil-ex-nohighlight))

(setq key-chord-two-keys-delay 0.5)
(key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
(key-chord-mode 1)

(define-key evil-normal-state-map "\C-e" 'move-end-of-line)
(define-key evil-insert-state-map "\C-e" 'move-end-of-line)
(define-key evil-visual-state-map "\C-e" 'move-end-of-line)
(define-key evil-motion-state-map "\C-e" 'move-end-of-line)
(define-key evil-normal-state-map "\C-f" 'forward-char)
(define-key evil-insert-state-map "\C-f" 'forward-char)
(define-key evil-visual-state-map "\C-f" 'forward-char)
(define-key evil-normal-state-map "\C-b" 'backward-char)
(define-key evil-insert-state-map "\C-b" 'backward-char)
(define-key evil-visual-state-map "\C-b" 'backward-char)
(define-key evil-normal-state-map "\C-d" 'delete-char)
(define-key evil-insert-state-map "\C-d" 'delete-char)
(define-key evil-visual-state-map "\C-d" 'delete-char)
(define-key evil-normal-state-map "\C-n" 'next-line)
(define-key evil-insert-state-map "\C-n" 'next-line)
(define-key evil-visual-state-map "\C-n" 'next-line)
(define-key evil-normal-state-map "\C-p" 'previous-line)
(define-key evil-insert-state-map "\C-p" 'previous-line)
(define-key evil-visual-state-map "\C-p" 'previous-line)
(define-key evil-normal-state-map "\C-w" 'delete)
(define-key evil-insert-state-map "\C-w" 'delete)
(define-key evil-visual-state-map "\C-w" 'delete)
(define-key evil-normal-state-map "\C-y" 'yank)
(define-key evil-insert-state-map "\C-y" 'yank)
(define-key evil-visual-state-map "\C-y" 'yank)
(define-key evil-normal-state-map "\C-k" 'kill-line)
(define-key evil-insert-state-map "\C-k" 'kill-line)
(define-key evil-visual-state-map "\C-k" 'kill-line)
;----------------------------------------------------------------------------

;----------------------------------------------------------------------------
; Org-Mod
;----------------------------------------------------------------------------
;(add-hook
; 'text-mode-hook
; 'auto-fill-mode)

(add-hook
 'text-mode-hook
 'olivetti-mode)
(setq olivetti-body-width 80)
(setq org-indent-indentation-per-level 1)
(setq org-adapt-indentation nil)

(setq org-src-tab-acts-natively t)

(setq org-hide-leading-stars t)
(setq org-hide-emphasis-markers t)

(customize-set-variable 'org-blank-before-new-entry
                        '((heading . nil)
                          (plain-list-item . nil)))
(setq org-cycle-separator-lines 1)


;(add-hook 'org-mode-hook (lambda ()
;  variable-pitch-mode 1 visual-line-mode))

(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(setq org-bullets-bullet-list '(" "))

(font-lock-add-keywords 'org-mode
   '(("^ *\\([-]\\) "
     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
(font-lock-add-keywords 'org-mode
   '(("^ *\\([+]\\) "
     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "◦"))))))

(setq-default prettify-symbols-alist '(("#+BEGIN_SRC" . "[")
  ("#+END_SRC" . "]")
  ("#+begin_src" . "[")
  ("#+end_src" . "]")
  (">=" . "≥")
  ("=>" . "⇨")))

(setq prettify-symbols-unprettify-at-point 'right-edge)
(add-hook 'org-mode-hook 'prettify-symbols-mode)

(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :weight bold :height 1.3))))
  '(org-level-2 ((t (:inherit outline-2 :weight bold :height 1.2))))
  '(org-level-3 ((t (:inherit outline-3 :weight bold :height 1.1))))
  '(org-level-4 ((t (:inherit outline-4 :weight bold :height 1.0))))
)
(custom-set-faces
   '(org-block-begin-line
     ((t (:background "#202630" :extend t))))
   '(org-block
     ((t (:background "#202630" :extend t))))
   '(org-block-end-line
     ((t (:background "#202630" :extend t))))
)
(add-hook 'org-mode-hook 'variable-pitch-mode)
(custom-theme-set-faces
 'user
 '(variable-pitch ((t (:family "Hack" :height 120))))
 '(fixed-pitch ((t ( :family "Fira Code Regular" :height 120)))))

;; html export options: no toc, no heading nu
(setq org-export-with-section-numbers nil)
(setq org-html-include-timestamps nil)
(setq org-export-with-toc nil)
;------------------------------------------------------------------------------
(map! :map global-map
      "<f8>"    #'treemacs
      "C-<f8>"  #'treemacs-select-window)

(setq treemacs-is-never-other-window t
          treemacs-width 30
          treemacs-show-hidden-files t)

(with-eval-after-load 'ess-r-mode
  (require 'ess-smart-underscore)

(find-file "~/Documents/org/note.org")
(find-file "~/Documents/org/Linux.org")
;(setq initial-buffer-choice "~/Documents/drafts")

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
